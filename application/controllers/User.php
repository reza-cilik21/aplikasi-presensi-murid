<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller{
     public function __construct(){
          parent::__construct();
          if (!$this->ion_auth->logged_in()) {
               $this->session->set_flashdata('info', 'No session found, please login first');
               redirect('auth');
          }
     }

     public function index(){
          $app['header'] = 'User';
          $app['title'] = 'Kelola User';
          $app['subtitle'] = 'Anda dapat mengatur semua user, seperti mendaftar, mengedit, menghapus dengan mudah';
          $app['contents'] = 'user/index';

          $this->db->select('*, users.id as id_user, groups.id as id_group');    
          $this->db->from('users');
          $this->db->join('users_groups', 'users_groups.user_id = users.id');
          $this->db->join('groups', 'groups.id = users_groups.group_id');
          $this->db->where('users.active = 1');
          $app['data'] = $query = $this->db->get();

          $this->load->view('utama_view', $app);
     }

     public function add(){
          $app['header'] = 'User';
          $app['title'] = 'Kelola User';
          $app['subtitle'] = 'Anda dapat mengatur semua user, seperti mendaftar, mengedit, menghapus dengan mudah';
          $app['contents'] = 'user/add';
          $this->load->view('utama_view', $app);
     }

     public function simpan(){
          $username = $this->input->post('email');
          $password = $this->input->post('no_hp');
          $email = $this->input->post('email');
          $additional_data = array(
               'first_name' => $this->input->post('nama'),
               'phone' => $this->input->post('no_hp'),
               'active' => 1
          );
          $group = array($this->input->post('hak_akses')); // Sets user to admin.

          $this->ion_auth->register($username, $password, $email, $additional_data, $group);

          $this->session->set_flashdata('success', 'Pengguna Baru Berhasi Ditambahkan');
          redirect('User');
     }     

     function hapus($id){
          $data = array(
               'active' => '0',
               );
          $this->ion_auth->update($id, $data);

          $this->session->set_flashdata('success', 'Pengumuman berhasil dihapus');

          redirect('User');
     }

}
