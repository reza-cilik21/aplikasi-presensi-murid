<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller{
     public function __construct(){
          parent::__construct();
     }

     public function index(){
          if (!$this->ion_auth->logged_in()) {
               $this->session->set_flashdata('info', 'No session found, please login first');
               redirect('auth');
          } else {
               $app['header'] = 'Dashboard';
               $app['title'] = 'Dashboard';
               $app['subtitle'] = 'halaman dashboard (contoh)';
               $app['contents'] = 'home';
               $this->load->view('utama_view', $app);
          }
     }
}
