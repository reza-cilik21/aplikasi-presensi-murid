<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TarikData extends CI_Controller
{
   public function __construct()
   {
      parent::__construct();
      if (!$this->ion_auth->logged_in()) {
         $this->session->set_flashdata('info', 'No session found, please login first');
         redirect('Auth');
      }
      $this->load->helper('uuid_helper');
      date_default_timezone_set('Asia/Jakarta');
   }

   public function index()
   {
      $app['header'] = 'Tarik Data Presensi';
      $app['title'] = 'Tarik Data Presensi';
      $app['subtitle'] = 'Tarik Data dari Mesin Presensi';
      $app['contents'] = 'tarik_data/index';

      $app['data_mesin'] = $query = $this->db->get('mesin');

      $this->db->select('*');
      $this->db->from('data_mesin_ditarik');
      $this->db->join('mesin', 'mesin.id_mesin = data_mesin_ditarik.id_mesin');
      $app['data_mesin_ditarik'] = $this->db->get();

      $this->load->view('utama_view', $app);
   }

   function tarikDataPresensi()
   {
      $app['header'] = 'Tarik Data Presensi';
      $app['title'] = 'Tarik Data Presensi';
      $app['subtitle'] = 'Tarik Data dari Mesin Presensi';
      $app['contents'] = 'tarik_data/tarik_data';

      $ip_address = "";
      $kunci = "";
      // $nama_mesin = "";
      $id_mesin = $this->input->post('id_mesin');
      $tanggal = $this->input->post('tanggal');

      $query = $this->db->get_where('mesin', array('id_mesin'=>$id_mesin));
      foreach($query->result() as $data_query){
         $ip_address = $data_query->ip_address;
         $kunci = $data_query->key_mesin;
         // $nama_mesin = $data_query->nama_mesin;
      }

      $status_koneksi = 0;
      $Connect = @fsockopen($ip_address, "80", $errno, $errstr, 1);
      if ($Connect) {
         $status_koneksi = 1;
         $soap_request = "<GetAttLog>
                              <ArgComKey xsi:type=\"xsd:integer\">" . $kunci . "</ArgComKey>
                              <Arg>
                                 <PIN xsi:type=\"xsd:integer\">All</PIN>
                              </Arg>
                          </GetAttLog>";

         $newLine = "\r\n";
         fputs($Connect, "POST /iWsService HTTP/1.0" . $newLine);
         fputs($Connect, "Content-Type: text/xml" . $newLine);
         fputs($Connect, "Content-Length: " . strlen($soap_request) . $newLine . $newLine);
         fputs($Connect, $soap_request . $newLine);
         $buffer = "";
         while ($Response = fgets($Connect, 1024)) {
            $buffer = $buffer . $Response;
         }
      } else $status_koneksi = 0;

      $buffer = @$this->Parse_Data($buffer, "<GetAttLogResponse>", "</GetAttLogResponse>");
      $buffer = explode("\r\n", $buffer);

      $export = array();
      $jml_data = 0;
      for ($a = 0; $a < count($buffer); $a++) {
         $data = $this->Parse_Data($buffer[$a], "<Row>", "</Row>");

         $tanggal_pilih = date('Y-m-d', strtotime($tanggal));
         $tanggal_mesin_pilih = date('Y-m-d', strtotime($this->Parse_Data($data, "<DateTime>", "</DateTime>")));

         if($tanggal_pilih == $tanggal_mesin_pilih){
            $export[$jml_data]['pin'] = $this->Parse_Data($data, "<PIN>", "</PIN>");
            $export[$jml_data]['waktu'] = $this->Parse_Data($data, "<DateTime>", "</DateTime>");
            // $export[$jml_data]['status'] = $this->Parse_Data($data, "<Status>", "</Status>");
            
            $query_siswa = $this->db->get_where('siswa', array('id_mesin'=>$id_mesin, 'kode_finger'=>$this->Parse_Data($data, "<PIN>", "</PIN>")));

            foreach($query_siswa->result() as $data_query_siswa){
               $export[$jml_data]['id_siswa']= $data_query_siswa->id_siswa;
               $export[$jml_data]['nis']= $data_query_siswa->nis;
               $export[$jml_data]['nama_siswa']= $data_query_siswa->nama_siswa;
            }
            
            $jml_data+=1;
         }

      }

      $app['tanggal_pilih'] = date('Y-m-d', strtotime($tanggal));
      $app['id_mesin'] = $id_mesin;
      $app['status_koneksi'] = $status_koneksi;
      $app['jml_data'] = $jml_data;
      $app['data_tarik'] = $export;
      $this->load->view('utama_view', $app);
   }

   function Parse_Data ($data,$p1,$p2) {
      $data = " ".$data;
      $hasil = "";
      $awal = strpos($data,$p1);
      if ($awal != "") {
         $akhir = strpos(strstr($data,$p1),$p2);
         if ($akhir != ""){
            $hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
         }
      }
      return $hasil;    
   }

   function simpanDataPresensi(){

      // CEK DATA SUDAH ADA ATAU BELUM AGAR TIDAK DUPLICATE
      $query_cek = $this->db->get_where('data_mesin_ditarik', array('tanggal_presensi' => $this->input->post('tanggal_pilih')));
      if($query_cek->num_rows() > 0){
         $id_data_mesin_ditarik = "";
         foreach($query_cek->result() as $data){
            $id_data_mesin_ditarik = $data->id_data_mesin_ditarik;
         }

         $this->db->delete('data_presensi', array('id_data_mesin_ditarik' => $id_data_mesin_ditarik));
         $this->db->delete('data_mesin_ditarik', array('tanggal_presensi' => $this->input->post('tanggal_pilih')));
      }

      $id_data_mesin_ditarik = uuid_v4();
      $data_mesin = array(
         'id_data_mesin_ditarik' => $id_data_mesin_ditarik,
         'id_mesin' => $this->input->post('id_mesin'),
         'waktu' => date('Y-m-d H:i:s'),
         'tanggal_presensi' => $this->input->post('tanggal_pilih')
      );
      $this->db->insert('data_mesin_ditarik', $data_mesin);


      $data_ditarik = json_decode(htmlspecialchars_decode($this->input->post('data_ditarik')), true);
      print_r($data_ditarik);
      for ($i = 0; $i < count($data_ditarik); $i++) { 
         $data = array(
            'id_data_presensi' => uuid_v4(),
            'id_siswa' => $data_ditarik[$i]['id_siswa'],
            'waktu' => $data_ditarik[$i]['waktu'],
            'id_data_mesin_ditarik' => $id_data_mesin_ditarik,
         );
   
         $this->db->insert('data_presensi', $data);
      }


      $this->session->set_flashdata('success', 'Data Presensi Berhasil Ditarik');
      redirect('TarikData');
      
   }

}
