<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa extends CI_Controller
{
   public function __construct()
   {
      parent::__construct();
      if (!$this->ion_auth->logged_in()) {
         $this->session->set_flashdata('info', 'No session found, please login first');
         redirect('Auth');
      }
      $this->load->helper('uuid_helper');
   }

   public function index()
   {
      $app['header'] = 'Data Siswa';
      $app['title'] = 'Kelola Data Siswa';
      $app['subtitle'] = 'Anda dapat mengatur data siswa, seperti menambah siswa baru dan mengedit dengan mudah';
      $app['contents'] = 'siswa/index';

      $this->db->select('*, siswa.status as status_siswa');
      $this->db->from('siswa');
      $this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
      $this->db->join('mesin', 'mesin.id_mesin = siswa.id_mesin');
      $this->db->order_by('kelas.nama_kelas');
      $this->db->order_by('siswa.status', 'desc');
      $this->db->order_by('siswa.nama_siswa');
      // $this->db->where('siswa.status = 1');
      $app['data'] = $this->db->get();

      $this->load->view('utama_view', $app);
   }

   public function add()
   {
      $app['header'] = 'Data Siswa';
      $app['title'] = 'Kelola Data Siswa';
      $app['subtitle'] = '<ul><li>Silakan input beberapa data berikut. Tanda * wajib diinputkan. </li> <li>Saat pertama kali kelas ditambah, status siswa otomatis Aktif. Anda dapat menonaktifkan di halaman utama Siswa</li></ul>';
      $app['contents'] = 'siswa/add';

      $app['data_kelas'] = $this->db->get('kelas');
      $app['data_mesin'] = $this->db->get('mesin');
      $this->load->view('utama_view', $app);
   }

   public function simpan()
   {

      $data = array(
         'id_siswa' => uuid_v4(),
         'id_mesin' => $this->input->post('id_mesin'),
         'id_kelas' => $this->input->post('id_kelas'),
         'kode_finger' => $this->input->post('kode_finger'),
         'nis' => $this->input->post('nis'),
         'nama_siswa' => $this->input->post('nama_siswa'),
         'status' => 1,
      );

      $this->db->insert('siswa', $data);

      $this->session->set_flashdata('success', 'Siswa Baru Berhasil Ditambahkan');
      redirect('Siswa');
   }

   function edit($id){
      $app['header'] = 'Data Siswa';
      $app['title'] = 'Edit Data Siswa';
      $app['subtitle'] = 'Tanda * wajib diinputkan';
      $app['contents'] = 'siswa/edit';

      $app['data_siswa'] = $query = $this->db->get_where('siswa', array('id_siswa' => $id));
      $app['data_kelas'] = $this->db->get('kelas');
      $app['data_mesin'] = $this->db->get('mesin');

      $this->load->view('utama_view', $app);
   }

   public function update()
   {
      $id_siswa = $this->input->post('id_siswa');
      $data_update = array(
         'id_mesin' => $this->input->post('id_mesin'),
         'id_kelas' => $this->input->post('id_kelas'),
         'kode_finger' => $this->input->post('kode_finger'),
         'nis' => $this->input->post('nis'),
         'nama_siswa' => $this->input->post('nama_siswa'),
      );

      $this->db->update('siswa', $data_update, array('id_siswa' => $id_siswa));

      $this->session->set_flashdata('success', 'Data Siswa Berhasil Diperbaharui');
      redirect('Siswa');
   }

   function ubah_status($id_siswa){
      $data_lama = $this->db->get_where('siswa', array('id_siswa' => $id_siswa));
      $status_lama = 0;
      foreach ($data_lama->result() as $data) { 
         $status_lama = $data->status;
      }

      $status_baru = ($status_lama == 0) ? 1 : 0;

      $data_update = array(
         'status' => $status_baru
      );

      $this->db->update('siswa', $data_update, array('id_siswa' => $id_siswa));

      $this->session->set_flashdata('success', 'Status Berhasil Diperbaharui');
      redirect('Siswa');
   }


}
