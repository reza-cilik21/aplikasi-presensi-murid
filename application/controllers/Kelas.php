<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas extends CI_Controller
{
   public function __construct()
   {
      parent::__construct();
      if (!$this->ion_auth->logged_in()) {
         $this->session->set_flashdata('info', 'No session found, please login first');
         redirect('Auth');
      }
      $this->load->helper('uuid_helper');
   }

   public function index()
   {
      $app['header'] = 'Data Kelas';
      $app['title'] = 'Kelola Data Kelas';
      $app['subtitle'] = 'Anda dapat mengatur kelas, seperti membuat kelas baru dan mengedit dengan mudah';
      $app['contents'] = 'kelas/index';

      $app['data'] = $query = $this->db->get('kelas');

      $this->load->view('utama_view', $app);
   }

   public function add()
   {
      $app['header'] = 'Data Kelas';
      $app['title'] = 'Tambah Data Kelas';
      $app['subtitle'] = '<ul><li>Silakan input beberapa data berikut. Tanda * wajib diinputkan. </li> <li>Saat pertama kali kelas ditambah, status kelas otomatis Aktif. Anda dapat menonaktifkan di halaman utama kelas</li></ul>';
      $app['contents'] = 'kelas/add';
      $this->load->view('utama_view', $app);
   }

   public function simpan()
   {

      $data = array(
         'id_kelas' => uuid_v4(),
         'tahun_ajaran' => $this->input->post('tahun_ajaran'),
         'nama_kelas' => $this->input->post('nama_kelas'),
         'status' => 1,
         'keterangan' => $this->input->post('keterangan'),

      );

      $this->db->insert('kelas', $data);

      $this->session->set_flashdata('success', 'Kelas Baru Berhasil Ditambahkan');
      redirect('Kelas');
   }

   function edit($id){
      $app['header'] = 'Data Kelas';
      $app['title'] = 'Edit Data Kelas';
      $app['subtitle'] = 'Tanda * wajib diinputkan';
      $app['contents'] = 'kelas/edit';

      $app['data'] = $query = $this->db->get_where('kelas', array('id_kelas' => $id));
      
      $this->load->view('utama_view', $app);
   }

   public function update()
   {
      $id_kelas = $this->input->post('id_kelas');
      $data_update = array(
         'tahun_ajaran' => $this->input->post('tahun_ajaran'),
         'nama_kelas' => $this->input->post('nama_kelas'),
         'keterangan' => $this->input->post('keterangan'),
      );

      $this->db->update('kelas', $data_update, array('id_kelas' => $id_kelas));

      $this->session->set_flashdata('success', 'Data kelas Berhasil Diperbaharui');
      redirect('Kelas');
   }

   function ubah_status($id_kelas){
      $data_lama = $this->db->get_where('kelas', array('id_kelas' => $id_kelas));
      $status_lama = 0;
      foreach ($data_lama->result() as $data) { 
         $status_lama = $data->status;
      }

      $status_baru = ($status_lama == 0) ? 1 : 0;

      $data_update = array(
         'status' => $status_baru
      );

      $this->db->update('kelas', $data_update, array('id_kelas' => $id_kelas));

      $this->session->set_flashdata('success', 'Status Berhasil Diperbaharui');
      redirect('Kelas');
   }

   
}
