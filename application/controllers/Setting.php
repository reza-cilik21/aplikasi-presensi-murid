<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends CI_Controller
{
   public function __construct()
   {
      parent::__construct();
      if (!$this->ion_auth->logged_in()) {
         $this->session->set_flashdata('info', 'No session found, please login first');
         redirect('Auth');
      }
      $this->load->helper('uuid_helper');
      date_default_timezone_set('Asia/Jakarta');
   }

   public function index()
   {
      redirect('Home');
   }

   function ganti_password()
   {
      $app['header'] = 'Ganti Password';
      $app['title'] = 'Ganti Password';
      $app['subtitle'] = 'Menu untuk Mengganti Password';
      $app['contents'] = 'setting/password';

      $app['data_mesin'] = $query = $this->db->get('mesin');

      $this->load->view('utama_view', $app);
   }

   function ganti_password_proses()
   {
      if ($this->input->post('new') == $this->input->post('new_confirm')) {
         $identity = $this->session->userdata('identity');

         $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

         if ($change) {
            //if the password was successfully changed
            $this->session->set_flashdata('message', 'Password Berhasil Diubah, Silakan Login dengan Password Baru');
            redirect('Auth/logout');
         } else {
            $this->session->set_flashdata('error', 'Password Lama Salah');
            redirect('Setting/ganti_password', 'refresh');
         }
      } else {
         $this->session->set_flashdata('error', 'Konfirmasi Password Baru Salah/Tidak Sama');
         redirect('Setting/ganti_password');
      }
   }
}
