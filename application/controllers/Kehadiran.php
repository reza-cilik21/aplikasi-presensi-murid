<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Csv;


class Kehadiran extends CI_Controller
{
   public function __construct()
   {
      parent::__construct();
      if (!$this->ion_auth->logged_in()) {
         $this->session->set_flashdata('info', 'No session found, please login first');
         redirect('Auth');
      }
      $this->load->helper('uuid_helper');
      date_default_timezone_set('Asia/Jakarta');
   }

   public function index()
   {
      $app['header'] = 'Rekap Kehadiran Presensi';
      $app['title'] = 'Rekap Kehadiran Presensi';
      $app['subtitle'] = 'Data Rekap Kehadiran Presensi';
      $app['contents'] = 'kehadiran/index';

      $app['kelas'] = $query = $this->db->get('kelas');

      $this->load->view('utama_view', $app);
   }

   function rekap()
   {
      $id_kelas = $this->input->post('id_kelas');
      $tgl = $this->input->post('tanggal');

      // $data_siswa = $this->db->get('siswa');
      $data_siswa = $this->db->get_where('siswa', array('id_kelas' => $id_kelas));

      $data_kelas = $this->db->get_where('kelas', array('id_kelas' => $id_kelas));
      $nama_kelas = "";
      foreach ($data_kelas->result() as $kelas) {
         $nama_kelas = $kelas->tahun_ajaran . ' - ' . $kelas->nama_kelas;
      }

      $date_awal = date_create($tgl);
      $app['tgl'] = date_format($date_awal, 'd F Y');
      $app['id_kelas'] = $id_kelas;
      $app['nama_kelas'] = $nama_kelas;
      $app['data_siswa'] = $data_siswa;
      $app['hadir'] = $this;
      $this->load->view('kehadiran/rekap', $app);
   }

   function jam_presensi($id_siswa, $tgl)
   {
      $tanggal = date_format(date_create($tgl), 'Y-m-d');
      $jam_presensi = array();

      $query_data_presensi = $this->db->query("SELECT * FROM data_presensi WHERE 
                                 id_siswa = '$id_siswa' AND 
                                 DATE(waktu) = '$tanggal'
                                 ORDER BY waktu
                                 ");
      $hitung = 1;
      foreach ($query_data_presensi->result() as $data) {
         if ($hitung == 1) {
            $jam_presensi[0] = date_format(date_create($data->waktu), 'H:i:s');
         } else if ($hitung > 1) {
            $jam_presensi[1] = date_format(date_create($data->waktu), 'H:i:s');;
         }

         $hitung++;
      }

      return $jam_presensi;
   }

   function export_data()
   {
      // ambil data dari form
      $date_awal = date_create($this->input->post('tanggal_awal'));
      $tanggal_awal = date_format($date_awal, 'd F Y');
      $date_akhir = date_create($this->input->post('tanggal_akhir'));
      $tanggal_akhir = date_format($date_akhir, 'd F Y');
      $id_kelas = $this->input->post('id_kelas');

      // ambil tahun ajaran dan nama kelas dari db
      $nama_kelas = "";
      $query_kelas = $this->db->get_where('kelas', array('id_kelas' => $id_kelas));
      foreach($query_kelas->result() as $data_kelas){
         $nama_kelas = $data_kelas->tahun_ajaran.' - '.$data_kelas->nama_kelas;
      }

      // memulai fungsi-fungsi untuk export excel
      $spreadsheet = new Spreadsheet();
      $sheet = $spreadsheet->getActiveSheet();
      $sheet->setCellValue('A1', 'Rekap Presensi Tanggal : '.$tanggal_awal.' s/d '.$tanggal_akhir);
      $sheet->setCellValue('A2', 'Tahun Ajaran - Kelas : '.$nama_kelas);

      $arrayData = array();

      //atur header tabel
      $arrayData[0][0] = "No";
      $arrayData[0][1] = "NIS";
      $arrayData[0][2] = "Nama Siswa";
      $arrayData[1][0] = "";
      $arrayData[1][1] = "";
      $arrayData[1][2] = "";
      $kolom = 3;
      $jumlah_hari = (date_diff($date_awal,$date_akhir)->d)+1; // +1 karena hari terakhir diikutkan
      
      for ($i=0; $i < $jumlah_hari*2; $i++) { 
         $arrayData[0][$kolom+$i] = date_format($date_awal,"d-m");
         $arrayData[0][$kolom+$i+1] = "";

         $arrayData[1][$kolom+$i] = "BRKT";
         $arrayData[1][$kolom+$i+1] = "PLG";
         
         date_add($date_awal,date_interval_create_from_date_string("1 days"));
         $i++;
      }
      
      // atur isi tabel
      $query_data_siswa = $this->db->get_where('siswa', array('id_kelas' => $id_kelas));
      $baris = 2; // index ke-0&1 untuk heading
      foreach($query_data_siswa->result() as $data_siswa){
         $arrayData[$baris][0] = $baris-1;
         $arrayData[$baris][1] = $data_siswa->nis;
         $arrayData[$baris][2] = $data_siswa->nama_siswa;
         
         $date_awal_isi = date_create($this->input->post('tanggal_awal'));
         $kolom = 3;
         for ($i=0; $i < $jumlah_hari*2; $i++) { 
            $tanggal = date_format($date_awal_isi, 'Y-m-d');
            $temp = $this->jam_presensi($data_siswa->id_siswa, $tanggal);
            $arrayData[$baris][$kolom+$i] = isset($temp[0]) ? $temp[0] : " - " ;
            $arrayData[$baris][$kolom+$i+1] = isset($temp[1]) ? $temp[1] : " - " ;

            date_add($date_awal_isi,date_interval_create_from_date_string("1 days"));
            $i++;
         }

         $baris++;
      }
      

      // print_r($arrayData);


      $spreadsheet->getActiveSheet()
         ->fromArray(
            $arrayData,  // The data to set
            NULL,        // Array values with this value will not be set
            'A4'         // Top left coordinate of the worksheet range where
            //    we want to set these values (default is A1)
         );

      
      $writer = new Xlsx($spreadsheet);
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="Rekap Presensi.xlsx"');
      header('Cache-Control: max-age=0');

      $writer->save('php://output');
   }
}
