<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mesin extends CI_Controller
{
   public function __construct()
   {
      parent::__construct();
      if (!$this->ion_auth->logged_in()) {
         $this->session->set_flashdata('info', 'No session found, please login first');
         redirect('Auth');
      }
      $this->load->helper('uuid_helper');
   }

   public function index()
   {
      $app['header'] = 'Mesin Presensi';
      $app['title'] = 'Kelola Mesin Presensi';
      $app['subtitle'] = 'Anda dapat mengatur semua user, seperti mendaftar, mengedit, menghapus dengan mudah';
      $app['contents'] = 'mesin/index';

      $app['data'] = $query = $this->db->get('mesin');

      $app['mesin'] = $this;

      $this->load->view('utama_view', $app);
   }

   public function add()
   {
      $app['header'] = 'Mesin Presensi';
      $app['title'] = 'Tambah Mesin Presensi';
      $app['subtitle'] = 'Anda dapat mengatur semua konfigurasi mesin presensi, seperti mendaftar, mengedit, menghapus dengan mudah. Tanda * wajib diinputkan';
      $app['contents'] = 'mesin/add';
      $this->load->view('utama_view', $app);
   }

   public function simpan()
   {

      $data = array(
         'id_mesin' => uuid_v4(),
         'nama_mesin' => $this->input->post('nama_mesin'),
         'ip_address' => $this->input->post('ip_address'),
         'key_mesin' => $this->input->post('key_mesin'),
         'keterangan' => $this->input->post('keterangan'),

      );

      $this->db->insert('mesin', $data);

      $this->session->set_flashdata('success', 'Konfigurasi Mesin Baru Berhasil Ditambahkan');
      redirect('Mesin');
   }

   function edit($id)
   {
      $app['header'] = 'Mesin Presensi';
      $app['title'] = 'Edit Mesin Presensi';
      $app['subtitle'] = 'Tanda * wajib diinputkan';
      $app['contents'] = 'mesin/edit';

      $app['data'] = $query = $this->db->get_where('mesin', array('id_mesin' => $id));

      $this->load->view('utama_view', $app);
   }

   public function update()
   {
      $id_mesin = $this->input->post('id_mesin');
      $data_update = array(
         'nama_mesin' => $this->input->post('nama_mesin'),
         'ip_address' => $this->input->post('ip_address'),
         'key_mesin' => $this->input->post('key_mesin'),
         'keterangan' => $this->input->post('keterangan'),

      );

      $this->db->update('mesin', $data_update, array('id_mesin' => $id_mesin));

      $this->session->set_flashdata('success', 'Konfigurasi Mesin Berhasil Diperbaharui');
      redirect('Mesin');
   }

   function hapus($id)
   {
      $this->db->delete('mesin', array("id_mesin" => $id));

      $this->session->set_flashdata('success', 'Konfigurasi Mesin Berhasil Dihapus');

      redirect('Mesin');
   }

   function koneksiMesin($ip_address, $kunci)
   {
      $status_koneksi = "";
      $Connect = fsockopen($ip_address, "80", $errno, $errstr, 1);
      if ($Connect) {
         $soap_request = "<GetAttLog>
         <ArgComKey xsi:type=\"xsd:integer\">" . $kunci . "</ArgComKey>
         <Arg>
         <PIN xsi:type=\"xsd:integer\">All</PIN>
         </Arg>
         </GetAttLog>";

         $newLine = "\r\n";
         fputs($Connect, "POST /iWsService HTTP/1.0" . $newLine);
         fputs($Connect, "Content-Type: text/xml" . $newLine);
         fputs($Connect, "Content-Length: " . strlen($soap_request) . $newLine . $newLine);
         fputs($Connect, $soap_request . $newLine);
         $buffer = "";
         while ($Response = fgets($Connect, 1024)) {
            $buffer = $buffer . $Response;
         }

         $buffer = @$this->Parse_Data($buffer, "<GetAttLogResponse>", "</GetAttLogResponse>");
         $buffer = explode("\r\n", $buffer);
         $jml_data = 0;
         for ($a = 0; $a < count($buffer); $a++) {
            $jml_data+=1;
         }

         $status_koneksi = "Berhasil; Jumlah Log Presensi = ".$jml_data." dari 200.000";
      } else {
         $status_koneksi = "Gagal";
      }

      return $status_koneksi;
   }

   function Parse_Data ($data,$p1,$p2) {
      $data = " ".$data;
      $hasil = "";
      $awal = strpos($data,$p1);
      if ($awal != "") {
         $akhir = strpos(strstr($data,$p1),$p2);
         if ($akhir != ""){
            $hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
         }
      }
      return $hasil;    
   }
}
