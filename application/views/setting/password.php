<div class="card">
   <div class="card-body">
      <h3 class="card-title"><?= $title ?></h3>
      <div class="card-subtitle"><?= $subtitle ?></div>

      <ul>
         <li>Anda dapat mengubah password pada menu ini, silakan update dengan password yang mudah diingat, tetapi sulit ditebak oleh orang lain</li>
         <li>PASSWORD HARAP DICATAT. Jangan sampai lupa, karena tidak ada fitur untuk 'Lupa Password' atau 'Reset Password'</li>

      </ul>

      <?php if ($this->session->flashdata('error') != '') { ?>
         <div class="alert alert-warning" role="alert">
            <h5><?php echo $this->session->flashdata('error'); ?></h5>
         </div>
      <?php } ?>

      <form class="form-material m-t-40" style="margin-top: 10px;" action="<?php echo site_url('Setting/ganti_password_proses'); ?>" method="post" enctype="multipart/form-data">
         <div class="form-group">
            <label>
               <h4>Password Lama</h4>
            </label>
            <input type="password" class="form-control form-control-line" name="old" required>
         </div>
         <div class="form-group">
            <label>
               <h4>Password Baru</h4>
            </label>
            <input type="password" class="form-control form-control-line" name="new" required>
         </div>
         <div class="form-group">
            <label>
               <h4>Konfirmasi Password Baru</h4>
            </label>
            <input type="password" class="form-control form-control-line" name="new_confirm" required>
         </div>

         <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Update Password</button>
      </form>

   </div>
</div>