<div class="card">
   <div class="card-body">
      <h3 class="card-title"><?= $title ?></h3>
      <div class="card-subtitle"><?= $subtitle ?></div>

      <ul>
         <li>Untuk melihat rekap data, pastikan Anda telah melakukan Penarikan Data Presensi terlebih dahulu yang tersedia pada menu "Tarik Data Presensi"</li>

      </ul>
      <br>

      <hr>

      <h2>Lihat Rekap Presensi Harian</h2>

      <form class="form-material m-t-40" style="margin-top: 10px;" action="<?php echo site_url('Kehadiran/rekap'); ?>" method="post" enctype="multipart/form-data" target="_blank">
         <div class="form-group">
            <label>
               <h5>Tahun Ajaran - Nama Kelas</h5>
            </label>
            <select class="js-example-basic-single form-control form-control-line" name="id_kelas" required>
               <option value="">Pilih Tahun Ajaran - Kelas</option>
               <?php foreach($kelas->result() as $data) { ?>
                  <option value="<?php echo $data->id_kelas;?>"><?php echo $data->tahun_ajaran.' - '.$data->nama_kelas; ?></option>
               <?php } ?>
            </select>
         </div>
         <div class="form-group">
            <label>
               <h5>Rentang Tanggal</h5>
            </label>
            <div class="row">
               <div class="col">
                  <input type="date" class="form-control form-control-line" name="tanggal" required>
               </div>
            </div>
         </div>

         <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" name="rekap" value="lihat">Lihat Rekap Presensi</button>
      </form>
      <br><br>
      <hr>

      <h2>Download Rekap Presensi Berdasarkan Rentang Waktu</h2>
      <form class="form-material m-t-40" style="margin-top: 10px;" action="<?php echo site_url('Kehadiran/export_data'); ?>" method="post" enctype="multipart/form-data" target="_blank">
         <div class="form-group">
            <label>
               <h5>Tahun Ajaran - Nama Kelas</h5>
            </label>
            <select class="js-example-basic-single form-control form-control-line" name="id_kelas" required>
               <option value="">Pilih Tahun Ajaran - Kelas</option>
               <?php foreach($kelas->result() as $data) { ?>
                  <option value="<?php echo $data->id_kelas;?>"><?php echo $data->tahun_ajaran.' - '.$data->nama_kelas; ?></option>
               <?php } ?>
            </select>
         </div>
         <div class="form-group">
            <label>
               <h5>Rentang Tanggal</h5>
            </label>
            <div class="row">
               <div class="col">
                  <input type="date" class="form-control form-control-line" name="tanggal_awal" required>
               </div>
               <div class="col">
                  <input type="date" class="form-control form-control-line" name="tanggal_akhir" required>
               </div>
            </div>
         </div>

         <button type="submit" class="btn btn-info waves-effect waves-light m-r-10" name="rekap" value="export">Download Ms. Excel</button>
      </form>

   </div>
</div>

<script>
   $(document).ready(function() {
      $('.js-example-basic-single').select2();
   });
</script>