<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Rekap Presensi Kelas</title>

   <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/laporan.css">
</head>

<body>

   <div>
      <table border="0" style="border: none;">
         <tr>
            <!-- <td width="20%" style="text-align: center;"><img src="<?php echo base_url(); ?>/assets/img/logo.jpg" width="50%" height="50%"></td> -->
            <td width="100%" style="text-align: center;">
               <h3>REKAP KEHADIRAN PRESENSI</h3>
               <h4>TAHUN AJARAN - KELAS : <?php echo $nama_kelas; ?></h4>
               <h4>TANGGAL : <?php echo $tgl; ?> </h4>
            </td>
         </tr>
      </table>
   </div>

   <table border="1" style="width: 80%; margin-left: auto; margin-right: auto;">
      <thead>
         <tr>
            <th>No</th>
            <th>NIS</th>
            <th>Nama Siswa</th>
            <th>Presensi Berangkat</th>
            <th>Presensi Pulang</th>
         </tr>
      </thead>
      <tbody>
         <?php $no = 1;
         foreach ($data_siswa->result() as $data) { 
            $temp = $hadir->jam_presensi($data->id_siswa, $tgl);  
         ?>

            <tr>
               <td><?php echo $no ?> </td>
               <td><?php echo $data->nis; ?></td>
               <td><?php echo $data->nama_siswa; ?></td>
               <td style="text-align: center;"><?php echo isset($temp[0]) ? $temp[0] : "Tidak Ada" ; ?></td>
               <td style="text-align: center;"><?php echo isset($temp[1]) ? $temp[1] : "Tidak Ada" ; ?></td>
               <!-- <td><?php echo $temp[1]; ?></td> -->
            </tr>
         <?php $no++;
         } ?>

      </tbody>
   </table>
</body>

</html>