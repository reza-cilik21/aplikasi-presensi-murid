<div class="card">
     <div class="card-body">
          <h3 class="card-title"><?= $title ?></h3>
          <div class="card-subtitle"><?= $subtitle ?></div>
          <a href="<?php echo site_url('User') ?>" class="btn btn-warning btn-rounded m-t-10 float-right">Kembali</a>
          <form class="form-material m-t-40" style="margin-top: 65px;" action="<?php echo site_url('user/simpan'); ?>" method="post" enctype="multipart/form-data">
               <div class="form-group">
                    <label>
                         <h5>Nama</h5>
                    </label>
                    <input type="text" class="form-control form-control-line" name="nama" required>
               </div>
               <div class="form-group">
                    <label>
                         <h5>Email (untuk username dan digunakan untuk login)</h5>
                    </label>
                    <input type="text" class="form-control form-control-line" name="email" required>
               </div>
               <div class="form-group">
                    <label>
                         <h5>No HP (password default, pengguna diminta langsung mengubah)</h5>
                    </label>
                    <input type="text" class="form-control form-control-line" name="no_hp" required>
               </div>
               <div class="form-group">
                    <label>
                         <h5>Pilih Level Hak Akses</h5>
                    </label>
                    <select name="hak_akses" class="form-control form-control-line" required>
                         <option value="2">General</option>
                         <option value="1">Super Admin</option>
                    </select>
               </div>

               <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Tambah Data</button>
               <button type="reset" class="btn btn-warning waves-effect waves-light">Reset</button>
          </form>
     </div>
</div>


