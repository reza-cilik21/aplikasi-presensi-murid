<div class="card">
	<div class="card-body">
		<h4 class="card-title"><?= $title ?></h4>
		<div class="card-subtitle"><?= $subtitle ?></div>
		
		<ul>
			<li>Tombol "Register Pengguna Baru" dapat digunakan untuk menambah pengguna baru</li>
			<li>Klik di Nama/yang satu barisnya untuk melihat detail Pengguna</li>
			<li>Data dapat diubah di menu pengguna itu sendiri</li>
			<li>
				<button class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button> digunakan untuk menghapus pengguna
			</li>
		</ul>


		<a href="<?= site_url('user/add') ?>" class="btn btn-primary btn-rounded m-t-10">Register Pengguna Baru</a>
		<br><br>
		<table class="table" data-show-toggle="false" data-expand-first="true" data-paging="true" data-filtering="true">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Level Pengguna</th>
					<th>Email</th>
					<th>No HP/Telp</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1; foreach ($data->result() as $data){ ?>
					<tr>
						<td><?php echo $no ?> </td>
						<td><?php echo $data->first_name.' '.$data->last_name ?></td>
						<td><?php echo $data->description ?></td>
						<td><?php echo $data->email ?></td>
						<td><?php echo $data->phone ?></td>
						<td>
							<?php if($data->id_group != 1) { ?>
							<a href="<?php echo site_url('user/hapus/').$data->id_user; ?>" class="btn btn-danger delete">Hapus</a>
							<?php } ?>
						</td>
					</tr>
				<?php $no++; }?>

			</tbody>
		</table>
	
	</div>
</div>

<script type="text/javascript">
	jQuery(function($){
		$('.table').footable({
			// "columns": $.get('columns.json'),
			// "rows": $.get('rows.json')
		});
		$('.table').trigger('footable_expand_all');
	});

	$(document).ready(function() {

		$(document).on('click', '.delete', function(e) {
	               e.preventDefault();
	               swal_confirm($(this).attr('href'));
	          });
	});
</script>