<div class="card">
   <div class="card-body">
      <h4 class="card-title"><?= $title ?></h4>
      <div class="card-subtitle"><?= $subtitle ?></div>

      <ul>
         <li>Tombol "Tambah Kelas Baru" dapat digunakan untuk menambah data kelas yang baru</li>
         <li>
            <button class="btn btn-info btn-sm"><i class="mdi mdi-delete"></i></button> digunakan untuk mengedit kelas yang ada
         </li>
         <li>
            <button class="btn btn-warning btn-sm"><i class="mdi mdi-delete"></i></button> digunakan untuk mengubah status "Aktif" atau "Tidak Aktif" suatu kelas
         </li>
      </ul>


      <a href="<?= site_url('Kelas/add') ?>" class="btn btn-primary btn-rounded m-t-10">Tambah Kelas Baru</a>
      <br><br>
      <table class="table" id="data_kelas" data-show-toggle="false" data-expand-first="true" data-paging="true" data-filtering="true">
         <thead>
            <tr>
               <th>No</th>
               <th>Tahun Ajaran</th>
               <th>Kelas</th>
               <th>Keterangan</th>
               <th>Status</th>
               <th>Aksi</th>
            </tr>
         </thead>
         <tbody>
            <?php $no = 1;
            foreach ($data->result() as $data) { ?>
               <tr>
                  <td><?php echo $no ?> </td>
                  <td><?php echo $data->tahun_ajaran; ?></td>
                  <td><?php echo $data->nama_kelas; ?></td>
                  <td><?php echo $data->keterangan; ?></td>
                  <td><?php echo ($data->status == 1) ? "<p class='text-primary'><b>Aktif</b></p>" : "<p class='text-danger'><b>Tidak Aktif</b></p>"; ?></td>
                  <td>
                     <a href="<?php echo site_url('Kelas/edit/') . $data->id_kelas; ?>" class="btn btn-info edit">Edit</a>
                     <a href="<?php echo site_url('Kelas/ubah_status/') . $data->id_kelas; ?>" class="btn btn-warning status">Ubah Status</a>
                  </td>
               </tr>
            <?php $no++;
            } ?>

         </tbody>
      </table>

   </div>
</div>


<script type="text/javascript">
   $(document).ready(function() {
      $('#data_kelas').DataTable();
   });
</script>