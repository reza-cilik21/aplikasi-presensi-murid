<div class="card">
   <div class="card-body">
      <h3 class="card-title"><?= $title ?></h3>
      <div class="card-subtitle"><?= $subtitle ?></div>
      <a href="<?php echo site_url('Kelas') ?>" class="btn btn-warning btn-rounded m-t-10 float-right">Kembali</a>
      <form class="form-material m-t-40" style="margin-top: 65px;" action="<?php echo site_url('Kelas/update'); ?>" method="post" enctype="multipart/form-data">
         <?php foreach ($data->result() as $data) { ?>
            <input type="hidden" value="<?php echo $data->id_kelas;?>" name="id_kelas" >
            <div class="form-group">
               <label>
                  <h5>Tahun Ajaran *</h5>
               </label>
               <input type="text" class="form-control form-control-line" name="tahun_ajaran" value="<?php echo $data->tahun_ajaran; ?>" required>
            </div>
            <div class="form-group">
               <label>
                  <h5>Nama Kelas *</h5>
               </label>
               <input type="text" class="form-control form-control-line" name="nama_kelas" value="<?php echo $data->nama_kelas; ?>" required>
            </div>
            <div class="form-group">
               <label>
                  <h5>Keterangan</h5>
               </label>
               <textarea class="form-control form-control-line" name="keterangan"> <?php echo $data->keterangan; ?></textarea>
            </div>


            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Update Data</button>
         <?php } ?>
      </form>
   </div>
</div>