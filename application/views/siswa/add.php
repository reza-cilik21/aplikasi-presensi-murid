<div class="card">
   <div class="card-body">
      <h3 class="card-title"><?= $title ?></h3>
      <div class="card-subtitle"><?= $subtitle ?></div>
      <a href="<?php echo site_url('Siswa') ?>" class="btn btn-warning btn-rounded m-t-10 float-right">Kembali</a>
      <form class="form-material m-t-40" style="margin-top: 65px;" action="<?php echo site_url('Siswa/simpan'); ?>" method="post" enctype="multipart/form-data">
         <div class="form-group">
            <label>
               <h5>NIS *</h5>
            </label>
            <input type="text" class="form-control form-control-line" name="nis" required>
         </div>
         <div class="form-group">
            <label>
               <h5>Nama Siswa *</h5>
            </label>
            <input type="text" class="form-control form-control-line" name="nama_siswa" required>
         </div>
         <div class="form-group">
            <label>
               <h5>Kode Finger *</h5>
            </label>
            <input type="text" class="form-control form-control-line" name="kode_finger" required>
         </div>
         <div class="form-group">
            <label>
               <h5>Pilih Mesin *</h5>
            </label>
            <select id="id_mesin" name="id_mesin" class="form-control form-control-line" required>
               <option value="">Pilih Mesin Finger</option>
               <?php
               foreach ($data_mesin->result() as $m) {
                  echo '<option value="' . $m->id_mesin . '" >' . $m->nama_mesin . '</option>';
               }
               ?>
            </select>
         </div>
         <div class="form-group">
            <label>
               <h5>Pilih Tahun Ajaran - Kelas *</h5>
            </label>
            <select class="js-example-basic-single form-control form-control-line" name="id_kelas" required>
               <option value="">Pilih Tahun Ajaran - Kelas</option>
               <?php foreach($data_kelas->result() as $data) { ?>
                  <option value="<?php echo $data->id_kelas; ?>"><?php echo $data->tahun_ajaran.' - '.$data->nama_kelas; ?></option>
               <?php } ?>
            </select>
            <!-- <select id="id_kelas" name="id_kelas" class="form-control form-control-line" required>
               <option value="">Pilih Kelas</option>
               <?php
               // foreach ($data_kelas->result() as $k) {
               //    echo '<option value="' . $k->id_kelas . '" >' . $k->nama_kelas . '</option>';
               // }
               ?>
            </select> -->
         </div>

         <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Tambah Data</button>
         <button type="reset" class="btn btn-warning waves-effect waves-light">Reset</button>
      </form>
   </div>
</div>

<script>
   $(document).ready(function() {
      $('.js-example-basic-single').select2();
   });
</script>