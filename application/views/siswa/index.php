<div class="card">
   <div class="card-body">
      <h4 class="card-title"><?= $title ?></h4>
      <div class="card-subtitle"><?= $subtitle ?></div>

      <ul>
         <li>Tombol "Tambah Siswa Baru" dapat digunakan untuk menambah data siswa yang baru</li>
         <li>
            Jika <b>siswa naik kelas</b> maka cukup diperbaharui menjadi kelas yang baru
         </li>
         <li>
            <button class="btn btn-info btn-sm"><i class="mdi mdi-delete"></i></button> digunakan untuk mengedit siswa yang ada
         </li>
         <li>
            <button class="btn btn-warning btn-sm"><i class="mdi mdi-delete"></i></button> digunakan untuk mengubah status "Aktif" atau "Tidak Aktif" dari siswa bersangkutan
         </li>
      </ul>


      <a href="<?= site_url('Siswa/add') ?>" class="btn btn-primary btn-rounded m-t-10">Tambah Siswa Baru</a>
      <br><br>
      <table class="table" id="data_siswa" data-show-toggle="false" data-expand-first="true" data-paging="true" data-filtering="true">
         <thead>
            <tr>
               <th>No</th>
               <th>NIS</th>
               <th>Nama</th>
               <th>Kode Finger</th>
               <th>Nama Mesin</th>
               <th>Kelas</th>
               <th>Status</th>
               <th>Aksi</th>
            </tr>
         </thead>
         <tbody>
            <?php $no = 1;
            foreach ($data->result() as $data) { ?>
               <tr>
                  <td><?php echo $no ?> </td>
                  <td><?php echo $data->nis; ?></td>
                  <td><?php echo $data->nama_siswa; ?></td>
                  <td><?php echo $data->kode_finger; ?></td>
                  <td><?php echo $data->nama_mesin; ?></td>
                  <td><?php echo $data->tahun_ajaran.' - '.$data->nama_kelas; ?></td>
                  <td><?php echo ($data->status_siswa == 1) ? "<p class='text-primary'><b>Aktif</b></p>" : "<p class='text-danger'><b>Tidak Aktif</b></p>"; ?></td>
                  <td>
                     <a href="<?php echo site_url('Siswa/edit/') . $data->id_siswa; ?>" class="btn btn-info edit">Edit</a>
                     <a href="<?php echo site_url('Siswa/ubah_status/') . $data->id_siswa; ?>" class="btn btn-warning status">Ubah Status</a>
                  </td>
               </tr>
            <?php $no++;
            } ?>

         </tbody>
      </table>

   </div>
</div>


<script type="text/javascript">
   $(document).ready(function() {
      $('#data_siswa').DataTable();
   });
</script>