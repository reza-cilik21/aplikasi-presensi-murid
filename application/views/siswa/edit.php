<div class="card">
   <div class="card-body">
      <h3 class="card-title"><?= $title ?></h3>
      <div class="card-subtitle"><?= $subtitle ?></div>
      <a href="<?php echo site_url('Siswa') ?>" class="btn btn-warning btn-rounded m-t-10 float-right">Kembali</a>
      <form class="form-material m-t-40" style="margin-top: 65px;" action="<?php echo site_url('Siswa/update'); ?>" method="post" enctype="multipart/form-data">
         <?php foreach ($data_siswa->result() as $data_siswa) { ?>
            <input type="hidden" name="id_siswa" value="<?php echo $data_siswa->id_siswa ?>">
            <div class="form-group">
               <label>
                  <h5>NIS *</h5>
               </label>
               <input type="text" class="form-control form-control-line" name="nis" value="<?php echo $data_siswa->nis; ?>" required>
            </div>
            <div class="form-group">
               <label>
                  <h5>Nama Siswa *</h5>
               </label>
               <input type="text" class="form-control form-control-line" name="nama_siswa" value="<?php echo $data_siswa->nama_siswa; ?>" required>
            </div>
            <div class="form-group">
               <label>
                  <h5>Kode Finger *</h5>
               </label>
               <input type="text" class="form-control form-control-line" name="kode_finger" value="<?php echo $data_siswa->kode_finger; ?>" required>
            </div>
            <div class="form-group">
               <label>
                  <h5>Pilih Mesin *</h5>
               </label>
               <select id="id_mesin" name="id_mesin" class="form-control form-control-line" required>
                  <option value="">Pilih Mesin Finger</option>

                  <?php foreach ($data_mesin->result() as $m) { ?>
                     <option value="<?php echo $m->id_mesin;?>" <?php if($m->id_mesin == $data_siswa->id_mesin) { echo 'selected="selected"'; }  ?> > <?php echo $m->nama_mesin;?> </option>
                  <?php } ?>

               </select>
            </div>
            <div class="form-group">
               <label>
                  <h5>Pilih Kelas *</h5>
               </label>
               <select class="js-example-basic-single form-control form-control-line" name="id_kelas" required>
               <!-- <select id="id_kelas" name="id_kelas" class="form-control form-control-line" required> -->
                  <option value="">Pilih Tahun Ajaran - Kelas</option>
                  <?php foreach ($data_kelas->result() as $k) { ?>
                     <option value="<?php echo $k->id_kelas;?>" <?php if($k->id_kelas == $data_siswa->id_kelas) { echo 'selected="selected"'; }  ?> > <?php echo $k->nama_kelas;?> </option>
                  <?php } ?>
               </select>
            </div>

            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Update Data</button>
            
         <?php } ?>
      </form>
   </div>
</div>

<script>
   $(document).ready(function() {
      $('.js-example-basic-single').select2();
   });
</script>