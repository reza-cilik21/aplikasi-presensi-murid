<div class="card">
   <div class="card-body">
      <h4 class="card-title"><?= $title ?></h4>
      <div class="card-subtitle"><?= $subtitle ?></div>
      <a href="<?php echo site_url('TarikData') ?>" class="btn btn-warning btn-rounded m-t-10 float-right">Kembali</a>

      <ul>
         <li>Silakan klik simpan untuk menyimpan ke Database</li>
         <li>Anda dapat melihat kehadiran per siswa/kelas/tanggal setelah menyimpan ke Database dan dapat dilihat di menu Daftar Hadir</li>
         <?php echo $tanggal_pilih ?>
      </ul>
      <br>

      <?php if ($status_koneksi == 1) { ?>
         
         <table class="table" data-show-toggle="false" data-expand-first="true" data-paging="true" data-filtering="true">
            <thead>
               <tr>
                  <th>No</th>
                  <th>Tanggal &amp; Waktu</th>
                  <th>Kode Finger</th>
                  <th>NIS</th>
                  <th>Nama Siswa</th>
               </tr>
            </thead>
            <tbody>
               <?php
               for ($i = 0; $i < $jml_data; $i++) {  ?>
                  <tr>
                     <td><?php echo $i + 1; ?> </td>
                     <td><?php echo $data_tarik[$i]['waktu']; ?></td>
                     <td><?php echo $data_tarik[$i]['pin']; ?></td>
                     <td><?php echo $data_tarik[$i]['nis']; ?></td>
                     <td><?php echo $data_tarik[$i]['nama_siswa']; ?></td>
                  </tr>
               <?php } ?>

               <?php if ($jml_data == 0) { ?>
                  <tr>
                     <td colspan="4" style="text-align: center;"><br>
                        <h3>Tidak Ada Data</h3>
                     </td>
                  </tr>
               <?php } ?>
            </tbody>
         </table>
         
         <?php if ($jml_data > 0) { ?>
         <form class="form-material m-t-40" action="<?php echo site_url('TarikData/simpanDataPresensi'); ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="data_ditarik" value="<?php echo htmlspecialchars(json_encode($data_tarik)); ?>">   
            <input type="hidden" name="id_mesin" value="<?php echo $id_mesin ?>">
            <input type="hidden" name="tanggal_pilih" value="<?php echo $tanggal_pilih ?>">

            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Simpan Ke Database</button>
         </form>
         <?php } ?>

      <?php } else { ?>
         <h3 style="text-align: center;">Mesin Presensi Tidak Terkoneksi/Mati</h3>
      <?php } ?>

      <?php
      // print_r($data_tarik);
      ?>

   </div>
</div>