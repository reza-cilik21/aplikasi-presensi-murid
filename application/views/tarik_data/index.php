<div class="card">
	<div class="card-body">
		<h4 class="card-title"><?= $title ?></h4>
		<div class="card-subtitle"><?= $subtitle ?></div>

		<ul>
			<li>Anda WAJIB menarik data presensi agar dapat menyimpan data presensi ke Database</li>
			<li>Jika ingin melihat kehadiran secara REALTIME maka harus dilakukan penarikan data terlebih dahulu. Setelah itu cek di menu Daftar Hadir </li> 
			<li>PASTIKAN Anda melakukan PENARIKAN KEMBALI di Sore Hari (setelah siswa presensi pulang). Hal ini akan mengupdate data presensi yang ada</li>
			<li>Jika anda LUPA MENARIK DATA di TANGGAL TERTENTU, anda tetap dapat menarik data presensi, tinggal pilih saja tanggal presensi yang diinginkan dan dilakukan cukup satu kali saja</li>
		</ul>

		<form class="form-material m-t-40" action="<?php echo site_url('TarikData/tarikDataPresensi'); ?>" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label>
					<h5>Nama Mesin</h5>
				</label>
				<select name="id_mesin" class="form-control form-control-line" required>
					<option value="">Pilih Mesin</option>
					<?php
					foreach ($data_mesin->result() as $data) {
						echo '<option value="' . $data->id_mesin . '" >' . $data->nama_mesin . '</option>';
					}
					?>
				</select>
			</div>
			<div class="form-group">
				<label>
					<h5>Tanggal Presensi</h5>
				</label>
				<input type="date" class="form-control form-control-line" name="tanggal" required>
			</div>

			<button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Verifikasi Data</button>
		</form>
		
		<hr>
		<br>
		<br>

		<h4>Data yang Telah Ditarik per Mesin </h4>
		<table class="table" id="data_tarik" data-show-toggle="false" data-expand-first="true" data-paging="true" data-filtering="true">
         <thead>
            <tr>
               <th>No</th>
               <th>Waktu Penarikan</th>
               <th>Nama Mesin</th>
               <th>Tanggal Presensi yang Ditarik</th>
               <!-- <th>Aksi</th> -->
            </tr>
         </thead>
         <tbody>
            <?php $no = 1;
            foreach ($data_mesin_ditarik->result() as $data) { ?>
               <tr>
                  <td><?php echo $no ?> </td>
                  <td><?php echo $data->waktu; ?></td>
                  <td><?php echo $data->nama_mesin; ?></td>
                  <td><?php echo $data->tanggal_presensi; ?></td>
                  <!-- <td>
                     <a href="<?php echo site_url('Siswa/edit/') . $data->id_siswa; ?>" class="btn btn-info edit">Edit</a>
                     <a href="<?php echo site_url('Siswa/ubah_status/') . $data->id_siswa; ?>" class="btn btn-warning status">Ubah Status</a>
                  </td> -->
               </tr>
            <?php $no++;
            } ?>

         </tbody>
      </table>

	</div>
</div>


<script type="text/javascript">
   $(document).ready(function() {
      $('#data_tarik').DataTable();
   });
</script>