<div class="card">
   <div class="card-body">
      <h3 class="card-title"><?= $title ?></h3>
      <div class="card-subtitle"><?= $subtitle ?></div>
      <a href="<?php echo site_url('Mesin') ?>" class="btn btn-warning btn-rounded m-t-10 float-right">Kembali</a>
      <form class="form-material m-t-40" style="margin-top: 65px;" action="<?php echo site_url('Mesin/update'); ?>" method="post" enctype="multipart/form-data">
         <?php foreach ($data->result() as $data) { ?>
            <input type="hidden" value="<?php echo $data->id_mesin;?>" name="id_mesin" >
            <div class="form-group">
               <label>
                  <h5>Nama Mesin *</h5>
               </label>
               <input type="text" class="form-control form-control-line" name="nama_mesin" value="<?php echo $data->nama_mesin; ?>" required>
            </div>
            <div class="form-group">
               <label>
                  <h5>IP Address *</h5>
               </label>
               <input type="text" class="form-control form-control-line" name="ip_address" value="<?php echo $data->ip_address; ?>" required>
            </div>
            <div class="form-group">
               <label>
                  <h5>Key Mesin *</h5>
               </label>
               <input type="text" class="form-control form-control-line" name="key_mesin" value="<?php echo $data->key_mesin; ?>" required>
            </div>
            <div class="form-group">
               <label>
                  <h5>Keterangan</h5>
               </label>
               <textarea class="form-control form-control-line" name="keterangan"><?php echo $data->keterangan; ?></textarea>
            </div>

            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Update Data</button>
         <?php } ?>
      </form>
   </div>
</div>