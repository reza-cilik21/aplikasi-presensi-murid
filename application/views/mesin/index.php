<div class="card">
	<div class="card-body">
		<h4 class="card-title"><?= $title ?></h4>
		<div class="card-subtitle"><?= $subtitle ?></div>

		<ul>
			<li>Tombol "Tambah Mesin Baru" dapat digunakan untuk menambah konfigurasi mesin presensi yang baru</li>
			<li>
				Tombol <button class="btn btn-info btn-sm">Edit<i class="mdi mdi-delete"></i></button> digunakan untuk mengedit konfigurasi mesin
			</li>
			<!-- <li>
				<button class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button> digunakan untuk menghapus konfigurasi mesin
			</li> -->
			<li>
				Untuk memastikan mesin terkoneksi dengan sepenuhnya harap menguji coba sebanyak 3 kali dan pastikan selalu "Berhasil". Jika ada "Gagal" satu kali saja, pastikan kembali IP Address dan Key Mesin yang diinputkan. Setiap kali mau menguji harap untuk melakukan refresh halaman terlebih dahulu sebelum klik tombol <button class="btn btn-success")>Cek Mesin</button>
			</li>
			<li>
				Informasi jumlah Log Presensi juga dapat dilihat setelah klik tombol <button class="btn btn-success")>Cek Mesin</button>. Jika sudah hampir habis (maksimal 200.000 transaksi) harap dihapus data Log Presensinya melalui mesin finger scan. SEBELUM MENGHAPUS pastikan data PRESENSI SUDAH DITARIK.
			</li>
		</ul>


		<a href="<?= site_url('Mesin/add') ?>" class="btn btn-primary btn-rounded m-t-10">Tambah Mesin Baru</a>
		<br><br>
		<table class="table" id="data_mesin" data-show-toggle="false" data-expand-first="true" data-paging="true" data-filtering="true">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Mesin</th>
					<th>IP Address</th>
					<th>Key Mesin</th>
					<th>keterangan</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1;
				foreach ($data->result() as $data) { ?>
					<tr>
						<td><?php echo $no ?> </td>
						<td><?php echo $data->nama_mesin; ?></td>
						<td><?php echo $data->ip_address; ?></td>
						<td><?php echo $data->key_mesin; ?></td>
						<td><?php echo $data->keterangan; ?></td>
						<td>
							<a href="<?php echo site_url('Mesin/edit/') . $data->id_mesin; ?>" class="btn btn-info edit">Edit</a>
							<!-- <a href="#" data-href="<?php echo site_url('Mesin/hapus/').$data->id_mesin; ?>" data-toggle="modal" data-target="#confirm-delete" class="btn btn-danger delete">Hapus</a> -->
							<button class="btn btn-success" onclick="alert('Koneksi <?php echo @$mesin->koneksiMesin($data->ip_address, $data->key_mesin); ?> ')">Cek Mesin</button>
						</td>
					</tr>
				<?php $no++;
				} ?>

			</tbody>
		</table>

	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Konfirmasi Hapus Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Apakah anda yakin ingin menghapus konfigurasi data mesin ini???
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<!-- <button type="button" class="btn btn-danger btn-ok">Hapus</button> -->
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});

	$(document).ready(function() {
      $('#data_mesin').DataTable();
   });

	// jQuery(function($) {
	// 	$('.table').footable({
	// 		// "columns": $.get('columns.json'),
	// 		// "rows": $.get('rows.json')
	// 	});
	// 	$('.table').trigger('footable_expand_all');
	// });

	// $(document).ready(function() {
	// 	$(document).on('click', '.delete', function(e) {
	// 		e.preventDefault();
	// 		swal_confirm($(this).attr('href'));
	// 	});
		
	// });


</script>