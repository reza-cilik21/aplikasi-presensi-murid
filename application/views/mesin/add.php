<div class="card">
     <div class="card-body">
          <h3 class="card-title"><?= $title ?></h3>
          <div class="card-subtitle"><?= $subtitle ?></div>
          <a href="<?php echo site_url('Mesin') ?>" class="btn btn-warning btn-rounded m-t-10 float-right">Kembali</a>
          <form class="form-material m-t-40" style="margin-top: 65px;" action="<?php echo site_url('Mesin/simpan'); ?>" method="post" enctype="multipart/form-data">
               <div class="form-group">
                    <label>
                         <h5>Nama Mesin *</h5>
                    </label>
                    <input type="text" class="form-control form-control-line" name="nama_mesin" required>
               </div>
               <div class="form-group">
                    <label>
                         <h5>IP Address *</h5>
                    </label>
                    <input type="text" class="form-control form-control-line" name="ip_address" required>
               </div>
               <div class="form-group">
                    <label>
                         <h5>Key Mesin *</h5>
                    </label>
                    <input type="text" class="form-control form-control-line" name="key_mesin" required>
               </div>
               <div class="form-group">
                    <label>
                         <h5>Keterangan</h5>
                    </label>
                    <textarea class="form-control form-control-line" name="keterangan"></textarea>
               </div>
               

               <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Tambah Data</button>
               <button type="reset" class="btn btn-warning waves-effect waves-light">Reset</button>
          </form>
     </div>
</div>


