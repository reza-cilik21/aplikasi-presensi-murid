<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Dashboard</h1>

</div>

<!-- Content Row -->
<div class="row">

	<!-- Earnings (Monthly) Card Example -->
	<!-- <div class="col-xl-4 col-md-6 mb-4">
		<div class="card border-left-primary shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Kelas Aktif</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-building fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	<!-- Earnings (Monthly) Card Example -->
	<!-- <div class="col-xl-4 col-md-6 mb-4">
		<div class="card border-left-success shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Siswa Aktif</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-users fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	<!-- Pending Requests Card Example -->
	<!-- <div class="col-xl-4 col-md-6 mb-4">
		<div class="card border-left-warning shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Jumlah Mesin Presensi</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800">18</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-table fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div> -->
</div>

<!-- Content Row -->


<div class="row">
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Selamat Datang</h6>
		</div>
		<div class="card-body">
			<p>
				Selamat datang di Aplikasi pengelola kehadiran siswa. Aplikasi ini terintegrasi dengan mesin finger scan. Alur-alur yang harus dikerjakan agar dapat menggunakan aplikasi ini adalah sebagai berikut:
			</p>
			<ol>
				<li>Daftarkan sidik jari siswa pada mesin finger scan yang telah ditentukan</li>
				<li>Atur pengaturan Mesin yang dapat dilakukan pada menu "Mesin"</li>
				<li>Atur pengaturan Kelas/Rombel yang dapat dilakukan pada menu "Kelas/Rombel"</li>
				<li>Masukkan data siswa-siswa sesuai dengan Kelas/Rombel pada menu "Data Siswa". Jika siswa nantinya naik kelas, jangan daftarkan/tambah data baru, cukup edit data kelasnya saja</li>
				<li>Pengaturan dan penambahan di tahapan atas merupakan pembuatan data master. Selanjutnya siswa tinggal melakukan presensi melalui mesin</li>
				<li>Jika anda ingin melihat kehadiran siswa, hal pertama yang harus dilakukan adalah melakukan penarikan data yang dapat dilakukan di menu "Tarik Data Presensi". Hal ini dilakukan untuk menyimpan data presensi siswa di mesin ke dalam database sistem aplikasi ini</li>
				<li>Setelah data presensi ditarik dari mesin, anda dapat melihat rekap kehadiran Berangkat dan Pulang pada menu "Rekap Daftar Hadir"</li>
			</ol>
		</div>
	</div>

</div>