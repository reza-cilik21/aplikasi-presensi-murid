-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2020 at 04:19 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aplikasi_presensi_murid`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_mesin_ditarik`
--

CREATE TABLE `data_mesin_ditarik` (
  `id_data_mesin_ditarik` char(36) NOT NULL,
  `id_mesin` char(36) NOT NULL,
  `waktu` datetime NOT NULL,
  `tanggal_presensi` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_mesin_ditarik`
--

INSERT INTO `data_mesin_ditarik` (`id_data_mesin_ditarik`, `id_mesin`, `waktu`, `tanggal_presensi`) VALUES
('0ccf2abc-a097-43e8-ba0c-a1397ad486a7', '29894d02-7b44-4de0-9547-63c582bf07fa', '2020-08-08 08:10:46', '2020-08-08'),
('7eab1af2-c5a0-473a-bb3f-75cd26664622', '29894d02-7b44-4de0-9547-63c582bf07fa', '2020-08-08 08:09:40', '2020-08-07'),
('9265b5f1-81af-49bd-b48e-c31852d1684f', '29894d02-7b44-4de0-9547-63c582bf07fa', '2020-08-12 07:06:28', '2020-08-12'),
('b629919a-283b-4860-9067-72e3122c5904', '29894d02-7b44-4de0-9547-63c582bf07fa', '2020-08-11 19:14:02', '2020-08-05');

-- --------------------------------------------------------

--
-- Table structure for table `data_presensi`
--

CREATE TABLE `data_presensi` (
  `id_data_presensi` char(36) NOT NULL,
  `id_data_mesin_ditarik` char(36) NOT NULL,
  `id_siswa` char(36) NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_presensi`
--

INSERT INTO `data_presensi` (`id_data_presensi`, `id_data_mesin_ditarik`, `id_siswa`, `waktu`) VALUES
('0f3b6f39-5b73-450a-bbd6-69f8c0ec23d9', '7eab1af2-c5a0-473a-bb3f-75cd26664622', '4a063514-d5e0-11ea-8d89-3cd92b56209e', '2020-08-07 07:00:57'),
('1836f090-8fcd-4a9c-b677-56bcab6f491f', '9265b5f1-81af-49bd-b48e-c31852d1684f', '4a063514-d5e0-11ea-8d89-3cd92b56209e', '2020-08-12 07:02:10'),
('1aabf8fc-7bb7-4c5c-b8f6-40f6e537c69a', '0ccf2abc-a097-43e8-ba0c-a1397ad486a7', '4a063514-d5e0-11ea-8d89-3cd92b56209e', '2020-08-08 07:45:55'),
('33a761dd-9cd1-46ba-83ef-ccfbf4969fd6', '9265b5f1-81af-49bd-b48e-c31852d1684f', '4a063514-d5e0-11ea-8d89-3cd92b56209e', '2020-08-12 07:06:08'),
('39984afc-7511-41e7-adc0-0e6c17843815', 'b629919a-283b-4860-9067-72e3122c5904', '4a063514-d5e0-11ea-8d89-3cd92b56209e', '2020-08-05 21:58:31'),
('39a138f1-3847-4d3a-acb6-28b3cb975f66', 'b629919a-283b-4860-9067-72e3122c5904', '4117dd1a-d5e0-11ea-8d89-3cd92b56209e', '2020-08-05 21:58:36'),
('3e198b07-4b87-4d6a-97ce-8e6ded7dec45', '9265b5f1-81af-49bd-b48e-c31852d1684f', '4a063514-d5e0-11ea-8d89-3cd92b56209e', '2020-08-12 05:53:19'),
('92bf133d-7fba-498a-827d-66640582bf96', '0ccf2abc-a097-43e8-ba0c-a1397ad486a7', '4a063514-d5e0-11ea-8d89-3cd92b56209e', '2020-08-08 08:09:00'),
('ab4e03d7-1077-4270-97d0-26370eff8d89', '0ccf2abc-a097-43e8-ba0c-a1397ad486a7', '4a063514-d5e0-11ea-8d89-3cd92b56209e', '2020-08-08 07:49:00'),
('cbace361-ab91-47f4-905e-443e523f7a5d', '7eab1af2-c5a0-473a-bb3f-75cd26664622', '4117dd1a-d5e0-11ea-8d89-3cd92b56209e', '2020-08-07 07:01:49'),
('eb3e1eb4-5e44-4735-b0d9-cd39e08f6c52', '7eab1af2-c5a0-473a-bb3f-75cd26664622', '4a063514-d5e0-11ea-8d89-3cd92b56209e', '2020-08-07 07:29:28'),
('f38a506f-912c-49f0-a703-3d2b71eb2fdb', '0ccf2abc-a097-43e8-ba0c-a1397ad486a7', '4117dd1a-d5e0-11ea-8d89-3cd92b56209e', '2020-08-08 08:09:59'),
('fb115383-4e3b-453d-acd3-528318cf46ad', '0ccf2abc-a097-43e8-ba0c-a1397ad486a7', '4117dd1a-d5e0-11ea-8d89-3cd92b56209e', '2020-08-08 06:38:51');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` char(36) NOT NULL,
  `tahun_ajaran` varchar(9) NOT NULL,
  `nama_kelas` varchar(36) NOT NULL,
  `keterangan` text NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `tahun_ajaran`, `nama_kelas`, `keterangan`, `status`) VALUES
('c475bd05-d065-11ea-9b4c-3cd92b56209e', '2019-2020', '10A', 'Kelas 10A', 1),
('d707cd39-faa0-4d1c-b3bb-6697669c1063', '2019-2021', '10BA', ' Kelas DuaA', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(2, '::1', 'sadad@fafsa.com', 1597325534),
(3, '::1', 'sada@dadas.ca', 1597325557);

-- --------------------------------------------------------

--
-- Table structure for table `mesin`
--

CREATE TABLE `mesin` (
  `id_mesin` char(36) NOT NULL,
  `nama_mesin` varchar(50) NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `key_mesin` int(10) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mesin`
--

INSERT INTO `mesin` (`id_mesin`, `nama_mesin`, `ip_address`, `key_mesin`, `keterangan`) VALUES
('29894d02-7b44-4de0-9547-63c582bf07fa', 'Mesin Satu', '192.168.100.201', 123456, 'Sebelah ruang kelas 10A'),
('ede91eee-d5e3-11ea-8d89-3cd92b56209e', 'Mesin Dua', '192.168.1.100', 654321, 'Samping kepsek');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` char(36) NOT NULL,
  `id_mesin` char(36) NOT NULL,
  `id_kelas` char(36) NOT NULL,
  `kode_finger` int(6) NOT NULL,
  `nis` varchar(15) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `id_mesin`, `id_kelas`, `kode_finger`, `nis`, `nama_siswa`, `status`) VALUES
('4117dd1a-d5e0-11ea-8d89-3cd92b56209e', '29894d02-7b44-4de0-9547-63c582bf07fa', 'c475bd05-d065-11ea-9b4c-3cd92b56209e', 1, '123456', 'Bejo Slamet Untung Subagyo', 1),
('4a063514-d5e0-11ea-8d89-3cd92b56209e', '29894d02-7b44-4de0-9547-63c582bf07fa', 'c475bd05-d065-11ea-9b4c-3cd92b56209e', 2, '456789', 'Agus', 1),
('d71cb697-3367-4aa1-b85f-f5f00d860f19', 'ede91eee-d5e3-11ea-8d89-3cd92b56209e', 'd707cd39-faa0-4d1c-b3bb-6697669c1063', 333, '333', '333', 0),
('f457c5d5-fd74-4aab-a7ae-aed2d9622b1e', '29894d02-7b44-4de0-9547-63c582bf07fa', 'c475bd05-d065-11ea-9b4c-3cd92b56209e', 3, '333333', 'Untung', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$p5LOiq2BHEaHBMcRL.pIPeS/jnIHzdruVnmvwTiXdQgkSKmzeL4Ma', 'sman1bandar@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1597325719, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_mesin_ditarik`
--
ALTER TABLE `data_mesin_ditarik`
  ADD PRIMARY KEY (`id_data_mesin_ditarik`),
  ADD KEY `tanggal_presensi` (`tanggal_presensi`);

--
-- Indexes for table `data_presensi`
--
ALTER TABLE `data_presensi`
  ADD PRIMARY KEY (`id_data_presensi`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mesin`
--
ALTER TABLE `mesin`
  ADD PRIMARY KEY (`id_mesin`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_mesin` (`id_mesin`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
